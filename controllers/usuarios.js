const { response, request } = require("express");
const bcryptjs = require("bcryptjs");

const Usuario = require("../models/usuario");
const { generarJWT } = require("../helpers/generar-jwt");

const usuariosGetAll = async (req = request, res = response) => {
  const { limite = 100, desde = 0 } = req.query;
  const query = { state: true }; // para solo traer usuarios con estado true, activos

  const [total, usuarios] = await Promise.all([
    Usuario.countDocuments(query), // para solo traer usuarios con estado true, activos
    Usuario.find(query).skip(Number(desde)).limit(Number(limite)),
  ]);

  res.json({
    total,
    usuarios,
  });
};

const usuariosGet = async (req, res = response) => {
  const { id } = req.params;

  const usuario = await Usuario.findById(id);

  res.json(usuario);
};

const usuariosPost = async (req, res = response) => {
  const {
    names,
    lastNames,
    // career,
    // university,
    // ..... y seguiamos con cada uno
    email,
    password,
    rol,
    ...resto // con esto ya solo trae todos los demas
  } = req.body;
  const usuario = new Usuario({
    names,
    lastNames,
    // career,
    // university,
    // ..... y seguiamos con cada uno
    email,
    password,
    rol,
    ...resto, // con esto ya solo trae todos los demas
  });

  // Encriptar la contraseña
  const salt = bcryptjs.genSaltSync(); //numero de vueltas para la encriptacion)mas vueltas mas complejo, por defecto esta en 10, como bcryptjs.genSaltSync(10)
  usuario.password = bcryptjs.hashSync(password, salt);

  // Guardar en BD
  await usuario.save();
  const token = await generarJWT(usuario.id);

  res.json({
    status: 1,
    usuario,
    token,
  });
};

const usuariosPut = async (req, res = response) => {
  const { id } = req.params;
  const { _id, password, google, email, ...resto } = req.body;
  // extrajimos email y google, para que no se pueda modificar estos datos
  // el password extrajimos para validar que exista y hacer su cambio

  //TODO validar contra base de datos

  if (password) {
    // el passwrod si llega significa que queremos cambiarlo

    // Encriptar la contraseña
    const salt = bcryptjs.genSaltSync();
    resto.password = bcryptjs.hashSync(password, salt);
  }

  const usuario = await Usuario.findByIdAndUpdate(id, resto, { new: true }); //busca el id y actualiza los datos, ademas se añadio {new:true}, para que retorne los valores actuales al cambio

  res.json(
    // msg: "put API -usuariosPut",
    { status: 1, usuario }
  );
  //   res.json({
  //     usuario, // si lo encierro en llaves, sale usuario:{} en la respuesta
  //   });
};

const usuariosPatch = (req, res = response) => {
  res.json({
    msg: "patch API - usuariosPatch",
  });
};

const usuariosDelete = async (req, res = response) => {
  const { id } = req.params;

  // Fisicamente lo borramos
  // const usuario = await Usuario.findByIdAndDelete( id ); //Esto si de verdad queremos quitarlo de la BD

  const usuario = await Usuario.findByIdAndUpdate(
    id,
    { state: false },
    { new: true }
  );

  res.json({ status: 1, usuario });
};

module.exports = {
  usuariosGetAll,
  usuariosGet,
  usuariosPost,
  usuariosPut,
  usuariosPatch,
  usuariosDelete,
};
