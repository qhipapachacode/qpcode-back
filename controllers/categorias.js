const { response } = require("express");
const { Categoria, Curso } = require("../models");

const obtenerCategorias = async (req, res = response) => {
  const { limite = 100, desde = 0 } = req.query;
  const query = { state: true };

  const [total, categorias] = await Promise.all([
    Categoria.countDocuments(query),
    Categoria.find(query)
      .populate("usuario", "lastNames")
      .skip(Number(desde))
      .limit(Number(limite)),
  ]);

  res.json({
    total,
    categorias,
  });
};

const obtenerCategoria = async (req, res = response) => {
  const { id } = req.params;
  const categoria = await Categoria.findById(id).populate(
    "usuario",
    "lastNames"
  );

  const { _id, nameCategory, usuario } = categoria;
  const cursos = await Curso.find({ category: _id.toString() });
  const cantidadCursos = cursos.length;
  const categoryComplete = {
    _id,
    nameCategory,
    usuario,
    cantidadCursos,
  };

  // res.json(categoria);
  res.json(categoryComplete);
};

const crearCategoria = async (req, res = response) => {
  const nameCategory = req.body.nameCategory.toUpperCase();

  const categoriaDB = await Categoria.findOne({ nameCategory });

  if (categoriaDB) {
    return res.status(400).json({
      msg: `La categoria ${categoriaDB.nameCategory}, ya existe`,
    });
  }

  // Generar la data a guardar
  const data = {
    nameCategory,
    usuario: req.usuario._id,
  };

  const categoria = new Categoria(data);

  // Guardar DB
  await categoria.save();

  res.status(201).json({ status: 1, categoria }); // SI SE CREA ALGo se manda el 201
};

const actualizarCategoria = async (req, res = response) => {
  const { id } = req.params;
  const { state, usuario, ...data } = req.body;
  const nameCategory = req.body.nameCategory.toUpperCase();

  const categoriaDB = await Categoria.findOne({ nameCategory });

  const categorySelf = await Categoria.findById(id);

  // si el nombre no sigue siendo el mismo en esa categoria
  if (nameCategory != categorySelf.nameCategory) {
    // si existe ya el nombre de ese curso y es unique

    if (categoriaDB) {
      return res.status(400).json({
        msg: `La categoria ${categoriaDB.nameCategory}, ya existe`,
      });
    }
  }

  data.nameCategory = data.nameCategory.toUpperCase();
  data.usuario = req.usuario._id;

  const categoria = await Categoria.findByIdAndUpdate(id, data, { new: true });

  res.json({ status: 1, categoria });
};

const borrarCategoria = async (req, res = response) => {
  const { id } = req.params;
  const categoriaBorrada = await Categoria.findByIdAndUpdate(
    id,
    { state: false },
    { new: true }
  );

  res.json({ status: 1, categoriaBorrada });
};

module.exports = {
  crearCategoria,
  obtenerCategorias,
  obtenerCategoria,
  actualizarCategoria,
  borrarCategoria,
};
