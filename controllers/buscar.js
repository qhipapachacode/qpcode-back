const { response } = require("express");
const { ObjectId } = require("mongoose").Types;

const { Usuario, Categoria, Curso } = require("../models");

const coleccionesPermitidas = ["usuarios", "categorias", "cursos", "roles"];

const buscarUsuarios = async (termino = "", res = response) => {
  const terminoFinal = termino.replaceAll("-", " ");

  const esMongoID = ObjectId.isValid(terminoFinal); // TRUE

  if (esMongoID) {
    const usuario = await Usuario.findById(terminoFinal);
    return res.json({
      results: usuario ? [usuario] : [],
    });
  }

  const regex = new RegExp(terminoFinal, "i");
  const usuarios = await Usuario.find({
    $or: [{ names: regex }, { email: regex }],
    $and: [{ state: true }],
  });

  res.json({
    results: usuarios,
  });
};

const buscarCategorias = async (termino = "", res = response) => {
  const esMongoID = ObjectId.isValid(termino); // TRUE

  if (esMongoID) {
    const categoria = await Categoria.findById(termino).populate(
      "usuario",
      "names"
    );
    return res.json({
      results: categoria ? [categoria] : [],
    });
  }

  const regex = new RegExp(termino, "i"); //hacemos que la expresion buscada sea insensible a mayusculas y minusculas
  const categorias = await Categoria.find({
    nameCategory: regex,
    state: true,
  }).populate("usuario", "names");

  res.json({
    results: categorias,
  });
};

const buscarCursos = async (termino = "", res = response) => {
  const terminoFinal = termino.replaceAll("-", " ");
  function isValidObjectID(id) {
    try {
      ObjectID.createFromHexString(id);
    } catch (e) {
      return false;
    }
    return true;
  }

  if (isValidObjectID(terminoFinal)) {
    const curso = await Curso.findById(terminoFinal)
      .populate("usuario", "names")
      .populate("category", "nameCategory");
    return res.json({
      results: curso ? [curso] : [],
    });
  }

  const regex = new RegExp(terminoFinal, "i");
  const cursos = await Curso.find({
    nameCourse: regex,
    state: true,
  })
    .populate("usuario", "names")
    .populate("category", "nameCategory")
    .populate("instructor", "names");

  res.json({
    results: cursos,
  });
};

const buscar = (req, res = response) => {
  const { coleccion, termino } = req.params;

  if (!coleccionesPermitidas.includes(coleccion)) {
    return res.status(400).json({
      msg: `Las colecciones permitidas son: ${coleccionesPermitidas}`,
    });
  }

  switch (coleccion) {
    case "usuarios":
      buscarUsuarios(termino, res);
      break;
    case "categorias":
      buscarCategorias(termino, res);
      break;
    case "cursos":
      buscarCursos(termino, res);
      break;

    default:
      res.status(500).json({
        msg: "Se le olvido hacer esta búsquda",
      });
  }
};

module.exports = {
  buscar,
};
