const { response } = require("express");
const { Curso, Usuario } = require("../models");

const obtenerCursos = async (req, res = response) => {
  const { limite = 100, desde = 0 } = req.query;
  const query = { state: true };

  const [total, cursos] = await Promise.all([
    Curso.countDocuments(query),
    Curso.find(query)
      .populate("usuario", "lastNames")
      .populate("category", "nameCategory")
      .populate("instructor", "lastNames")

      .skip(Number(desde))
      .limit(Number(limite)),
  ]);

  res.json({
    total,
    cursos,
  });
};

const obtenerCurso = async (req, res = response) => {
  const { id } = req.params;
  const curso = await Curso.findById(id)
    .populate("usuario", "lastNames")
    .populate("category", "nameCategory")
    .populate("instructor", "lastNames");

  res.json(curso);
};

const crearCurso = async (req, res = response) => {
  const { state, usuario, ...body } = req.body;

  const nameCourse = body.nameCourse.toUpperCase(); //lo agregue a q sea en mayuscula para reconcoer el curso que queria agregar, y me salga el error de que existe,ya que el guardado anterior lo hacia con mayuscula

  // const cursoDB = await Curso.findOne({ nameCourse: body.nameCourse }); //solo este no m funcionaba
  const cursoDB = await Curso.findOne({ nameCourse }); //lo agregue a q sea en mayuscula para reconocer el curso que queria agregar, y me salga el error de que existe,ya que el guardado anterior lo hacia con mayuscula

  if (cursoDB) {
    return res.status(400).json({
      msg: `El curso ${cursoDB.nameCourse}, ya existe`,
    });
  }

  // Generar la data a guardar
  const data = {
    ...body,
    nameCourse: body.nameCourse.toUpperCase(),
    usuario: req.usuario._id,
  };

  const curso = new Curso(data);

  // Guardar DB
  await curso.save();

  res.status(201).json({ status: 1, curso });
};

const actualizarCurso = async (req, res = response) => {
  const { id } = req.params;
  const { state, usuario, ...data } = req.body;
  const nameCourse = req.body.nameCourse.toUpperCase();

  const cursoDB = await Curso.findOne({ nameCourse });

  const cursoSelf = await Curso.findById(id);

  // si el nombre no sigue siendo el mismo en ese curso
  if (nameCourse != cursoSelf.nameCourse) {
    // si existe ya el nombre de ese curso y es unique
    if (cursoDB) {
      return res.status(400).json({
        msg: `El curso ${cursoDB.nameCourse}, ya existe`,
      });
    }
  }

  if (data.nameCourse) {
    data.nameCourse = data.nameCourse.toUpperCase();
  }

  data.usuario = req.usuario._id;

  const curso = await Curso.findByIdAndUpdate(id, data, { new: true })
    .populate("usuario", "lastNames")
    .populate("category", "nameCategory");

  res.json({ status: 1, curso });
};

const borrarCurso = async (req, res = response) => {
  const { id } = req.params;
  const cursoBorrado = await Curso.findByIdAndUpdate(
    id,
    { state: false },
    { new: true }
  );

  res.json({ status: 1, cursoBorrado });
};

module.exports = {
  crearCurso,
  obtenerCursos,
  obtenerCurso,
  actualizarCurso,
  borrarCurso,
};
