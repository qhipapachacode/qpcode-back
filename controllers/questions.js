const { response } = require("express");
const { Question } = require("../models");

const obtenerQuestions = async (req, res = response) => {
  const { limite = 100, desde = 0 } = req.query;
  const query = { state: true };

  const [total, questions] = await Promise.all([
    Question.countDocuments(query),
    Question.find(query)
      .populate("usuario", "lastNames")
      .skip(Number(desde))
      .limit(Number(limite)),
  ]);

  res.json({
    total,
    questions,
  });
};

const obtenerQuestion = async (req, res = response) => {
  const { id } = req.params;
  const question = await Question.findById(id).populate("usuario", "lastNames");
  res.json(question);
};

const crearQuestion = async (req, res = response) => {
  const { state, usuario, ...body } = req.body;
  const titleQuestion = req.body.titleQuestion;

  const questionDB = await Question.findOne({ titleQuestion });

  if (questionDB) {
    return res.status(400).json({
      msg: `La question ${questionDB.titleQuestion}, ya existe`,
    });
  }

  // Generar la data a guardar
  const data = {
    ...body,
    titleQuestion: body.titleQuestion,
    usuario: req.usuario._id,
  };

  const question = new Question(data);

  // Guardar DB
  await question.save();

  res.status(201).json({ status: 1, question }); // SI SE CREA ALGo se manda el 201
};

const actualizarQuestion = async (req, res = response) => {
  const { id } = req.params;
  const { state, usuario, ...data } = req.body;
  const titleQuestion = req.body.titleQuestion;

  const questionDB = await Question.findOne({ titleQuestion });

  const questionSelf = await Question.findById(id);

  // si el nombre no sigue siendo el mismo en esa question
  if (titleQuestion != questionSelf.titleQuestion) {
    // si existe ya el nombre de ese curso y es unique

    if (questionDB) {
      return res.status(400).json({
        msg: `La questionDB ${questionDB.titleQuestion}, ya existe`,
      });
    }
  }

  data.titleQuestion = data.titleQuestion;
  data.usuario = req.usuario._id;

  const question = await Question.findByIdAndUpdate(id, data, {
    new: true,
  }).populate("usuario", "lastNames");

  res.json({ status: 1, question });
};

const borrarQuestion = async (req, res = response) => {
  const { id } = req.params;
  const questionBorrada = await Question.findByIdAndUpdate(
    id,
    { state: false },
    { new: true }
  );

  res.json({ status: 1, questionBorrada });
};

module.exports = {
  crearQuestion,
  obtenerQuestions,
  obtenerQuestion,
  actualizarQuestion,
  borrarQuestion,
};
