const { response } = require("express");
const bcryptjs = require("bcryptjs");
const Usuario = require("../models/usuario");
const { sendMail } = require("../helpers/email-sender");
const { generateString } = require("../helpers/random-string.js");

const restorePass = async (req, res = response) => {
  const { email } = req.body;
  const usuario = await Usuario.findOne({ email });

  try {
    if (usuario === null) {
      res.json({
        status: 0,
        msg: "El email no se encuentra registrado",
      });
    } else {
      const password = generateString();
      // console.log(password);
      // Encriptar la contraseña
      const salt = bcryptjs.genSaltSync(); //numero de vueltas para la encriptacion)mas vueltas mas complejo, por defecto esta en 10, como bcryptjs.genSaltSync(10)
      usuario.password = bcryptjs.hashSync(password, salt);
      await usuario.save();

      const result = await sendMail(usuario.email, password);
      if (result === true) {
        res.json({
          status: 1,
          msg: "Envío de correo exitoso",
        });
      } else {
        res.json({
          status: -1,
          msg: "Error al enviar correo",
        });
      }
    }
  } catch {
    res.json({
      status: -1,
      msg: "Error en el servidor",
    });
  }
};

module.exports = {
  restorePass,
};
