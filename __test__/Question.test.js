const axios = require("axios");
const { generateString } = require("../helpers/random-string.js");

describe("Questions tests", () => {
  let QuestionsMock;
  let QuestionsMockNew;
  let token;
  let idUser;
  let idQuestions;
  let invalidID;
  let validID;

  beforeAll(async () => {
    QuestionsMock = {
      titleQuestion: `Questions ${generateString()}`,
    };
    QuestionsMockNew = {
      titleQuestion: `Questions ${generateString()}`,
    };
    invalidID = "12323asda";
    validID = "1234567890abcdefghij1234";
    // Login to get token
    const instance = axios.create({
      // Nota: tuve problemas al correr en local 8082, asi que en 5000 me funciono
      baseURL: "http://localhost:5000",
      timeout: 1000,
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });

    let data = {
      email: "jorge150896@hotmail.com",
      password: "123456",
    };

    try {
      const login = await instance.post("api/auth/login", data);
      const { token: tokenUser, usuario: idUserMongo } = login.data;
      token = tokenUser;
      idUser = idUserMongo.uid;
    } catch (error) {
      console.log(error.response);
    }
    console.log("token", token);
  });

  // ✅✔️✅
  it("Success create a Question ", async () => {
    const result = await axios.post(
      "http://localhost:5000/api/questions",
      QuestionsMock,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );

    idQuestions = result.data.question._id;

    expect(result.data).toBeDefined();
    expect(result.status).toEqual(201);
    expect(result.data.status).toEqual(1);
    expect(result.data.question.titleQuestion).toEqual(
      QuestionsMock.titleQuestion
    );
    // Valido que elusuario logueado es quien lo creo
    expect(result.data.question.usuario).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(result.data.question._id.length).toBe(24);
  });
  // ❌🔴❌
  it("Error create a Question Without Name", async () => {
    try {
      await axios.post(
        "http://localhost:5000/api/questions",
        { titleQuestion: "" },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("El nombre es obligatorio");
    }
  });
  // ❌🔴❌
  it("Error create a Question With name already registered", async () => {
    try {
      await axios.post("http://localhost:5000/api/questions", QuestionsMock, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.msg).toContain(
        `La question ${QuestionsMock.titleQuestion}, ya existe`
      );
    }
  });
  // ✅✔️✅

  it("Success get all Questions", async () => {
    const result = await axios.get("http://localhost:5000/api/questions");
    expect(result.status).toEqual(200);
    expect(Array.isArray(result.data.questions)).toEqual(true);
    expect(result.data.questions.length).toBeGreaterThan(0);
  });

  // ✅✔️✅

  it("Success get one Question", async () => {
    const questionGet = await axios.get(
      `http://localhost:5000/api/questions/${idQuestions}`
    );

    expect(questionGet.status).toEqual(200);
    expect(questionGet.data.titleQuestion).toEqual(QuestionsMock.titleQuestion);
    // Valido que elusuario logueado es quien lo creo
    expect(questionGet.data.usuario._id).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(questionGet.data._id.length).toBe(24);
  });

  // ❌🔴❌

  it("Error get one Question with ID invalid", async () => {
    try {
      await axios.get(`http://localhost:5000/api/questions/${invalidID}`);
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      // Valido que la cnatida de letras y texto que componen el id no son 24
      expect(data.errors[0].value.length).not.toBe(24);
    }
  });

  // ❌🔴❌

  it("Error get one Question with ID Valid Non-existent ", async () => {
    try {
      await axios.get(`http://localhost:5000/api/questions/${validID}`);
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      expect(data.errors[0].value.length).toBe(24);
    }
  });

  // ✅✔️✅
  it("Success Update one Question", async () => {
    const questionUpdate = await axios.put(
      `http://localhost:5000/api/questions/${idQuestions}`,
      QuestionsMockNew,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    expect(questionUpdate.status).toEqual(200);
    expect(questionUpdate.data.status).toEqual(1);
    expect(questionUpdate.data.question.titleQuestion).toEqual(
      QuestionsMockNew.titleQuestion
    );
    // Valido que elusuario logueado es quien lo actualizo
    expect(questionUpdate.data.question.usuario._id).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(questionUpdate.data.question._id.length).toBe(24);
  });
  // ❌🔴❌

  it("Error Update one Question Without Name", async () => {
    try {
      await axios.put(
        `http://localhost:5000/api/questions/${idQuestions}`,
        { titleQuestion: "" },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("El nombre es obligatorio");
    }
  });

  // ✅✔️✅

  it("Success Delete one Question", async () => {
    // search id to test
    const questionDelete = await axios.delete(
      `http://localhost:5000/api/questions/${idQuestions}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );

    expect(questionDelete.status).toEqual(200);
    expect(questionDelete.data.status).toEqual(1);
    expect(questionDelete.data.questionBorrada.titleQuestion).toEqual(
      QuestionsMockNew.titleQuestion
    );
    // Valido que elusuario logueado es quien lo elimino
    expect(questionDelete.data.questionBorrada.usuario).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(questionDelete.data.questionBorrada._id.length).toBe(24);
  });

  // ❌🔴❌

  it("Error Delete one Question with ID invalid", async () => {
    try {
      await axios.delete(`http://localhost:5000/api/questions/${invalidID}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);

      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      // Valido que la cnatida de letras y texto que componen el id no son 24
      expect(data.errors[0].value.length).not.toBe(24);
    }
  });

  // ❌🔴❌

  it("Error Delete one Question with ID Valid Non-existent ", async () => {
    try {
      await axios.delete(`http://localhost:5000/api/questions/${validID}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);

      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      // Valido que la cnatida de letras y texto que componen el id no son 24
      expect(data.errors[0].value.length).toBe(24);
    }
  });
});
