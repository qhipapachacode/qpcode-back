const axios = require("axios");
const { generateString } = require("../helpers/random-string.js");

describe("Courses tests", () => {
  let CursosMock;
  let CursosMockNew;
  let token;
  let idUser;
  let idCursos;
  let invalidID;
  let validID;

  beforeAll(async () => {
    CursosMock = {
      nameCourse: `Cursos ${generateString()}`,
      category: "628008b80e68334f328af8cd",
      instructor: "628008550e68334f328af8c9",
    };
    CursosMockNew = {
      nameCourse: `Cursos ${generateString()}`,
      category: "626375db39549a73261997fd",
      instructor: "628008550e68334f328af8c9",
    };
    invalidID = "12323asda";
    validID = "1234567890abcdefghij1234";
    // Login to get token
    const instance = axios.create({
      // Nota: tuve problemas al correr en local 8082, asi que en 5000 me funciono
      baseURL: "http://localhost:5000",
      timeout: 1000,
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });

    let data = {
      email: "jorge150896@hotmail.com",
      password: "123456",
    };

    try {
      const login = await instance.post("api/auth/login", data);
      const { token: tokenUser, usuario: idUserMongo } = login.data;
      token = tokenUser;
      idUser = idUserMongo.uid;
    } catch (error) {
      console.log(error.response);
    }
    console.log("token", token);
  });

  // ✅✔️✅
  it("Success create a Course ", async () => {
    const result = await axios.post(
      "http://localhost:5000/api/cursos",
      CursosMock,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );

    idCursos = result.data.curso._id;

    expect(result.data).toBeDefined();
    expect(result.status).toEqual(201);
    expect(result.data.status).toEqual(1);
    expect(result.data.curso.nameCourse).toEqual(
      CursosMock.nameCourse.toUpperCase()
    );
    expect(result.data.curso.category).toEqual(CursosMock.category);
    expect(result.data.curso.outstanding).toBe(false);

    // Valido que elusuario logueado es quien lo creo
    expect(result.data.curso.usuario).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(result.data.curso._id.length).toBe(24);
  });
  // ❌🔴❌
  it("Error create a Course Without Name", async () => {
    try {
      await axios.post(
        "http://localhost:5000/api/cursos",
        { nameCourse: "" },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("El nombre es obligatorio");
    }
  });

  // ❌🔴❌
  it("Error create a Course With name already registered", async () => {
    try {
      await axios.post("http://localhost:5000/api/cursos", CursosMock, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.msg).toContain(
        `El curso ${CursosMock.nameCourse.toUpperCase()}, ya existe`
      );
    }
  });

  // ✅✔️✅

  it("Success get all Courses", async () => {
    const result = await axios.get("http://localhost:5000/api/cursos");
    expect(result.status).toEqual(200);
    expect(Array.isArray(result.data.cursos)).toEqual(true);
    expect(result.data.cursos.length).toBeGreaterThan(0);
  });

  // ✅✔️✅

  it("Success get one Course", async () => {
    const cursoGet = await axios.get(
      `http://localhost:5000/api/cursos/${idCursos}`
    );

    expect(cursoGet.status).toEqual(200);
    expect(cursoGet.data.nameCourse).toEqual(
      CursosMock.nameCourse.toUpperCase()
    );
    expect(cursoGet.data.category._id).toEqual(CursosMock.category);
    expect(cursoGet.data.outstanding).toBe(false);
    // Valido que elusuario logueado es quien lo creo
    expect(cursoGet.data.usuario._id).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(cursoGet.data._id.length).toBe(24);
  });

  // ❌🔴❌

  it("Error get one Course with ID invalid", async () => {
    try {
      await axios.get(`http://localhost:5000/api/cursos/${invalidID}`);
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      // Valido que la cnatida de letras y texto que componen el id no son 24
      expect(data.errors[0].value.length).not.toBe(24);
    }
  });

  // ❌🔴❌

  it("Error get one Course with ID Valid Non-existent ", async () => {
    try {
      await axios.get(`http://localhost:5000/api/cursos/${validID}`);
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      expect(data.errors[0].value.length).toBe(24);
    }
  });

  // ✅✔️✅
  it("Success Update one Course", async () => {
    const cursoUpdate = await axios.put(
      `http://localhost:5000/api/cursos/${idCursos}`,
      CursosMockNew,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    expect(cursoUpdate.status).toEqual(200);
    expect(cursoUpdate.data.status).toEqual(1);
    expect(cursoUpdate.data.curso.nameCourse).toEqual(
      CursosMockNew.nameCourse.toUpperCase()
    );
    // Valido que elusuario logueado es quien lo actualizo
    expect(cursoUpdate.data.curso.usuario._id).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(cursoUpdate.data.curso._id.length).toBe(24);
  });

  // ❌🔴❌

  it("Error Update one Course Without Name", async () => {
    try {
      await axios.put(
        `http://localhost:5000/api/cursos/${idCursos}`,
        { nameCourse: "" },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("El nombre es obligatorio");
    }
  });
  // ✅✔️✅

  it("Success Delete one Course", async () => {
    // search id to test
    const cursoDelete = await axios.delete(
      `http://localhost:5000/api/cursos/${idCursos}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );

    expect(cursoDelete.status).toEqual(200);
    expect(cursoDelete.data.status).toEqual(1);
    expect(cursoDelete.data.cursoBorrado.nameCourse).toEqual(
      CursosMockNew.nameCourse.toUpperCase()
    );
    // Valido que elusuario logueado es quien lo elimino
    expect(cursoDelete.data.cursoBorrado.usuario).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(cursoDelete.data.cursoBorrado._id.length).toBe(24);
  });

  // ❌🔴❌

  it("Error Delete one Course with ID invalid", async () => {
    try {
      await axios.delete(`http://localhost:5000/api/cursos/${invalidID}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);

      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      // Valido que la cnatida de letras y texto que componen el id no son 24
      expect(data.errors[0].value.length).not.toBe(24);
    }
  });

  // ❌🔴❌

  it("Error Delete one Course with ID Valid Non-existent ", async () => {
    try {
      await axios.delete(`http://localhost:5000/api/cursos/${validID}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);

      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      // Valido que la cnatida de letras y texto que componen el id no son 24
      expect(data.errors[0].value.length).toBe(24);
    }
  });
});
