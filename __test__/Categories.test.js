const axios = require("axios");
const { generateString } = require("../helpers/random-string.js");

describe("Categories tests", () => {
  let CategoriesMock;
  let CategoriesMockNew;
  let token;
  let idUser;
  let idCategories;
  let invalidID;
  let validID;

  beforeAll(async () => {
    CategoriesMock = {
      nameCategory: `Categories ${generateString()}`,
    };
    CategoriesMockNew = {
      nameCategory: `Categories ${generateString()}`,
    };
    invalidID = "12323asda";
    validID = "1234567890abcdefghij1234";
    // Login to get token
    const instance = axios.create({
      // Nota: tuve problemas al correr en local 8082, asi que en 5000 me funciono
      baseURL: "http://localhost:5000",
      timeout: 1000,
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });

    let data = {
      email: "jorge150896@hotmail.com",
      password: "123456",
    };

    try {
      const login = await instance.post("api/auth/login", data);
      const { token: tokenUser, usuario: idUserMongo } = login.data;
      token = tokenUser;
      idUser = idUserMongo.uid;
    } catch (error) {
      console.log(error.response);
    }
    console.log("token", token);
  });

  // ✅✔️✅
  it("Success create a Categories ", async () => {
    const result = await axios.post(
      "http://localhost:5000/api/categorias",
      CategoriesMock,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );

    idCategories = result.data.categoria._id;

    expect(result.data).toBeDefined();
    expect(result.status).toEqual(201);
    expect(result.data.status).toEqual(1);
    expect(result.data.categoria.nameCategory).toEqual(
      CategoriesMock.nameCategory.toUpperCase()
    );
    // Valido que elusuario logueado es quien lo creo
    expect(result.data.categoria.usuario).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(result.data.categoria._id.length).toBe(24);
  });

  // ❌🔴❌
  it("Error create a Categorie Without Name", async () => {
    try {
      await axios.post(
        "http://localhost:5000/api/categorias",
        { nameCategory: "" },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("El nombre es obligatorio");
    }
  });

  // ❌🔴❌
  it("Error create a Categorie With name already registered", async () => {
    try {
      await axios.post("http://localhost:5000/api/categorias", CategoriesMock, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.msg).toContain(
        `La categoria ${CategoriesMock.nameCategory.toUpperCase()}, ya existe`
      );
    }
  });

  // ✅✔️✅

  it("Success get all Categories", async () => {
    const result = await axios.get("http://localhost:5000/api/categorias");
    expect(result.status).toEqual(200);
    expect(Array.isArray(result.data.categorias)).toEqual(true);
    expect(result.data.categorias.length).toBeGreaterThan(0);
  });

  // ✅✔️✅

  it("Success get one Categorie", async () => {
    const CategorieGet = await axios.get(
      `http://localhost:5000/api/categorias/${idCategories}`
    );

    expect(CategorieGet.status).toEqual(200);
    expect(CategorieGet.data.nameCategory).toEqual(
      CategoriesMock.nameCategory.toUpperCase()
    );
    // Valido que elusuario logueado es quien lo creo
    expect(CategorieGet.data.usuario.uid).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(CategorieGet.data._id.length).toBe(24);
  });

  // ❌🔴❌

  it("Error get one Categorie with ID invalid", async () => {
    try {
      await axios.get(`http://localhost:5000/api/categorias/${invalidID}`);
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      // Valido que la cnatida de letras y texto que componen el id no son 24
      expect(data.errors[0].value.length).not.toBe(24);
    }
  });

  // ❌🔴❌

  it("Error get one Categorie with ID Valid Non-existent ", async () => {
    try {
      await axios.get(`http://localhost:5000/api/categorias/${validID}`);
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      expect(data.errors[0].value.length).toBe(24);
    }
  });

  // ✅✔️✅
  it("Success Update one Categorie", async () => {
    const categorieUpdate = await axios.put(
      `http://localhost:5000/api/categorias/${idCategories}`,
      CategoriesMockNew,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    expect(categorieUpdate.status).toEqual(200);
    expect(categorieUpdate.data.status).toEqual(1);
    expect(categorieUpdate.data.categoria.nameCategory).toEqual(
      CategoriesMockNew.nameCategory.toUpperCase()
    );
    // Valido que elusuario logueado es quien lo actualizo
    expect(categorieUpdate.data.categoria.usuario).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(categorieUpdate.data.categoria._id.length).toBe(24);
  });

  // ❌🔴❌

  it("Error Update one Categorie Without Name", async () => {
    try {
      await axios.put(
        `http://localhost:5000/api/categorias/${idCategories}`,
        { nameCategory: "" },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);
      expect(data.errors[0].msg).toContain("El nombre es obligatorio");
    }
  });

  // ✅✔️✅

  it("Success Delete one Categorie", async () => {
    // search id to test
    const categorieDelete = await axios.delete(
      `http://localhost:5000/api/categorias/${idCategories}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );

    expect(categorieDelete.status).toEqual(200);
    expect(categorieDelete.data.status).toEqual(1);
    expect(categorieDelete.data.categoriaBorrada.nameCategory).toEqual(
      CategoriesMockNew.nameCategory.toUpperCase()
    );
    // Valido que elusuario logueado es quien lo elimino
    expect(categorieDelete.data.categoriaBorrada.usuario).toEqual(idUser);
    // Valido que la cnatida de letras y texto que componen el id sean 24
    expect(categorieDelete.data.categoriaBorrada._id.length).toBe(24);
  });

  // ❌🔴❌

  it("Error Delete one Categorie with ID invalid", async () => {
    try {
      await axios.delete(`http://localhost:5000/api/categorias/${invalidID}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);

      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      // Valido que la cnatida de letras y texto que componen el id no son 24
      expect(data.errors[0].value.length).not.toBe(24);
    }
  });

  // ❌🔴❌

  it("Error Delete one Categorie with ID Valid Non-existent ", async () => {
    try {
      await axios.delete(`http://localhost:5000/api/categorias/${validID}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
    } catch ({ response }) {
      const { status, data } = response;
      expect(status).toEqual(400);

      expect(data.errors[0].msg).toContain("No es un id de Mongo válido");
      // Valido que la cnatida de letras y texto que componen el id no son 24
      expect(data.errors[0].value.length).toBe(24);
    }
  });
});
