const { Schema, model } = require("mongoose");

const UsuarioSchema = Schema({
  names: {
    type: String,
    required: [true, "El nombre es obligatorio"],
  },
  lastNames: {
    type: String,
    // required: [true, "El apellido es obligatorio"],
    default: "",
  },
  career: {
    type: String,
    default: "",
  },
  university: {
    type: String,
    default: "",
  },
  actualWork: {
    type: String,
    default: "",
  },
  nameCompany: {
    type: String,
    default: "",
  },
  linkCV: {
    type: String,
    default: "",
  },
  address: {
    type: String,
    default: "",
  },
  phone: {
    type: String,
    default: "",
  },
  description: {
    type: String,
    default: "",
  },
  redLinkedin: {
    type: String,
    default: "",
  },
  redGitLab: {
    type: String,
    default: "",
  },
  redGitHub: {
    type: String,
    default: "",
  },
  redYoutube: {
    type: String,
    default: "",
  },
  redInsta: {
    type: String,
    default: "",
  },
  redFace: {
    type: String,
    default: "",
  },

  TecnologyOne: {
    type: String,
    default: "",
  },
  TecnologyTwo: {
    type: String,
    default: "",
  },
  TecnologyThree: {
    type: String,
    default: "",
  },

  email: {
    type: String,
    required: [true, "El email es obligatorio"],
    unique: true,
  },
  password: {
    type: String,
    required: [true, "La contraseña es obligatoria"],
  },
  img: {
    type: String,
    default: "",
  },
  activeMember: {
    type: Boolean,
    default: false,
  },
  activeInstructor: {
    type: Boolean,
    default: false,
  },
  rol: {
    type: String,
    required: true,
    emun: ["ADMIN_ROLE", "USER_ROLE"],
  },
  state: {
    type: Boolean,
    default: true,
  },
  google: {
    type: Boolean,
    default: false,
  },
});

//Debe ser funcion normal y no arrow function
UsuarioSchema.methods.toJSON = function () {
  const { __v, password, _id, ...usuario } = this.toObject(); //Extraigo los que no quiero mostrar, que son password y __v
  usuario.uid = _id;
  return usuario;
};

module.exports = model("Usuario", UsuarioSchema);
