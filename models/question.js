const { Schema, model } = require("mongoose");

const QuestionSchema = Schema({
  titleQuestion: {
    type: String,
    required: [true, "El nombre es obligatorio"],
    unique: true,
  },

  responseQuestion: {
    type: String,
    default: "",
  },

  state: {
    type: Boolean,
    default: true,
    required: true,
  },

  usuario: {
    //para saber que usuario creo la categoria
    type: Schema.Types.ObjectId,
    ref: "Usuario",
    required: true,
  },
});

QuestionSchema.methods.toJSON = function () {
  const { __v, state, ...data } = this.toObject();
  return data;
};

module.exports = model("Question", QuestionSchema);
