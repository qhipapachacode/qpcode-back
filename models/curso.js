const { Schema, model } = require("mongoose");

const CursoSchema = Schema({
  nameCourse: {
    type: String,
    required: [true, "El nombre es obligatorio"],
    unique: true,
  },
  state: {
    type: Boolean,
    default: true,
    required: true,
  },
  usuario: {
    type: Schema.Types.ObjectId,
    ref: "Usuario",
    required: true,
  },
  description: {
    type: String,
    default: "",
  },
  img: {
    type: String,
    default: "",
  },
  LinkYoutube: {
    type: String,
    default: "",
  },
  outstanding: {
    type: Boolean,
    default: false,
  },
  available: {
    type: Boolean,
    defult: true,
  },
  category: {
    type: Schema.Types.ObjectId,
    ref: "Categoria",
    required: true,
  },

  instructor: {
    type: Schema.Types.ObjectId,
    ref: "Usuario",
    required: true,
  },
});

CursoSchema.methods.toJSON = function () {
  const { __v, state, ...data } = this.toObject();
  return data;
};

module.exports = model("Curso", CursoSchema);
