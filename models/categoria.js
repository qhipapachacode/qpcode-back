const { Schema, model } = require("mongoose");

const CategoriaSchema = Schema({
  nameCategory: {
    type: String,
    required: [true, "El nombre es obligatorio"],
    unique: true,
  },
  state: {
    type: Boolean,
    default: true,
    required: true,
  },
  img: {
    type: String,
    default: "",
  },
  usuario: {
    //para saber que usuario creo la categoria
    type: Schema.Types.ObjectId,
    ref: "Usuario",
    required: true,
  },
});

CategoriaSchema.methods.toJSON = function () {
  const { __v, state, ...data } = this.toObject();
  return data;
};

module.exports = model("Categoria", CategoriaSchema);
