const Categoria = require("./categoria");
const Curso = require("./curso");
const Role = require("./role");
const Question = require("./question");
const Usuario = require("./usuario");

module.exports = {
  Categoria,
  Curso,
  Role,
  Question,
  Usuario,
};
