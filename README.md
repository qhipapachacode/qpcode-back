# QhipaPachaCode Backend

### [API Despliegue](https://qpcode.herokuapp.com "API QPCode")

### [API Documentation](https://documenter.getpostman.com/view/17190637/UVsHV8mb "API Documentation")

QPCode es una plataforma Web para la comunidad "Qhipa Pacha Code", grupo donde se toca temas de programación, innovación, diseño y emprendimiento; asimismo, brinda conocimientos a estudiantes de diversas partes del Perú, sus intengrantes participan en concursos, mejorarando sus habilidades 🚀

## Installation

Requires [Node.js](https://nodejs.org/) v16+ to run.

Install the dependencies and devDependencies and start the server.

```
npm install
```

For production environments...

```
PORT=
MONGODB_CNN=
SECRETORPRIVATEKEY
GOOGLE_CLIENT_ID
GOOGLE_SECRET_ID
CLOUDINARY_URL
SENDGRID_API_KEY
```

## Run

Development:

```sh
npm run dev
```

Production:

```sh
npm start
```

Test:

```sh
npm run server-test
npm run test
```

## Branchs

- nodeRestserver-configInitial-v1
- nodeRestserver-loginCrud-v2
- nodeRestserver-jwt-v3
- nodeRestserver-google-v4
- nodeRestserver-CRUDandSearch-v5
- nodeRestserver-optimizacionesv6
- nodeRestserver-uploadImages-v7
- nodeRestServer-fixes-v8
- nodeRestServer-addQuestions-v9

# configurar variables de entorno en Heroku

- heroku config <!--  ver variables de entorno -->
- heroku config:set MONGODB_CNN="" <!--  agregar variable de entorno -->
- heroku config:unset MONGODB_CNN <!--  para borrarlo -->
- heroku logs -n 100 <!-- Ver el numero de logs, los ultimos 100 -->
- heroku logs -n 100 --tail <!-- Ver el numero de logs, los ultimos 100 en tiempo real-->

## License

MIT

## :+1: Sígueme en mis redes sociales:

- WebPersonal: (https://jorge-vicuna.gitlab.io/jorge-vicuna/)
- GitLab: (https://gitlab.com/jorge_vicuna)
- Youtube: (https://www.youtube.com/channel/UCW0m1TKKiN3Etejqx6h3Jtg)
- Linkedin: (https://www.linkedin.com/in/jorge-vicuna-valle/)
- Facebook: (https://www.facebook.com/jorge.vicunavalle/)

<table>
    <td align="center" >
      <a href="https://jorge-vicuna.gitlab.io/jorge-vicuna/">
        <img src="https://jorge-vicuna.gitlab.io/jorge-vicuna/static/media/avatar.272f0e79.jpg" width="100px;" alt=""/>
        <br />
        <sub><b>Jorge Vicuña Valle</b></sub>
      </a>
            <br />
      <span>♌🍗🎸🏀</span>
    </td>
</Table>
