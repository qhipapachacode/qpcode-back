const { Router } = require("express");
const { check } = require("express-validator");

const {
  validarCampos,
  validarArchivoSubir,
  validarJWT,
  tieneRole,
} = require("../middlewares");
const {
  cargarArchivo,
  actualizarImagen,
  mostrarImagen,
  actualizarImagenCloudinary,
} = require("../controllers/uploads");
const { coleccionesPermitidas } = require("../helpers");

const router = Router();

router.post("/", [validarArchivoSubir], cargarArchivo);

router.put(
  "/:coleccion/:id",
  [
    validarJWT,
    tieneRole("ADMIN_ROLE", "USER_ROLE"), //SON LOS ROLES QUE PODRAN ELIMINAR UN USUARIO

    validarArchivoSubir,
    check("id", "El id debe de ser de mongo").isMongoId(),
    check("coleccion").custom((c) =>
      coleccionesPermitidas(c, ["usuarios", "cursos", "categorias"])
    ),
    validarCampos,
  ],
  actualizarImagenCloudinary
);
//   ],
//   actualizarImagen //si queremos guardar en carpeta de prueba uploads
// );

router.get(
  "/:coleccion/:id",
  [
    check("id", "El id debe de ser de mongo").isMongoId(),
    check("coleccion").custom((c) =>
      coleccionesPermitidas(c, ["usuarios", "cursos"])
    ),
    validarCampos,
  ],
  mostrarImagen
);

module.exports = router;
