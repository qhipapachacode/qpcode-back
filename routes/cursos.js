const { Router } = require("express");
const { check } = require("express-validator");

const {
  validarJWT,
  validarCampos,
  esAdminRole,
  tieneRole,
} = require("../middlewares");

const {
  crearCurso,
  obtenerCursos,
  obtenerCurso,
  actualizarCurso,
  borrarCurso,
} = require("../controllers/cursos");

const {
  existeCategoriaPorId,
  existeCursoPorId,
} = require("../helpers/db-validators");

const router = Router();

/**
 * {{url}}/api/categorias
 */

//  Obtener todas las categorias - publico
router.get("/", obtenerCursos);

// Obtener una categoria por id - publico
router.get(
  "/:id",
  [
    check("id", "No es un id de Mongo válido").isMongoId(),
    validarCampos,
    check("id").custom(existeCursoPorId),
    validarCampos,
  ],
  obtenerCurso
);

// Crear categoria - privado - cualquier persona con un token válido
router.post(
  "/",
  [
    validarJWT,
    tieneRole("ADMIN_ROLE"), //SON LOS ROLES QUE PODRAN ELIMINAR UN USUARIO

    check("nameCourse", "El nombre es obligatorio").not().isEmpty(),
    validarCampos,
    check("category", "La categoria es obligatoria").not().isEmpty(),
    validarCampos,
    check("instructor", "El instructor es obligatorio").not().isEmpty(),
    validarCampos,
    check("category", "No es un id de Mongo").isMongoId(),
    validarCampos,
    check("category").custom(existeCategoriaPorId),
    validarCampos,
  ],
  crearCurso
);

// Actualizar - privado - cualquiera con token válido
router.put(
  "/:id",
  [
    validarJWT,
    tieneRole("ADMIN_ROLE"), //SON LOS ROLES QUE PODRAN ELIMINAR UN USUARIO

    check("nameCourse", "El nombre es obligatorio").not().isEmpty(),
    validarCampos,
    check("category", "La categoria es obligatoria").not().isEmpty(),
    validarCampos,
    check("instructor", "El instructor es obligatorio").not().isEmpty(),
    validarCampos,
    // check('categoria','No es un id de Mongo').isMongoId(),
    check("id", "No es un id de Mongo válido").isMongoId(),
    validarCampos,
    check("id").custom(existeCursoPorId),

    validarCampos,
  ],
  actualizarCurso
);

// Borrar una categoria - Admin
router.delete(
  "/:id",
  [
    validarJWT,
    // esAdminRole,
    tieneRole("ADMIN_ROLE"), //SON LOS ROLES QUE PODRAN ELIMINAR UN USUARIO
    check("id", "No es un id de Mongo válido").isMongoId(),
    validarCampos,
    check("id").custom(existeCursoPorId),
    validarCampos,
  ],
  borrarCurso
);

module.exports = router;
