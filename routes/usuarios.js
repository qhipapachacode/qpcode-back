const { Router } = require("express");
const { check } = require("express-validator");

const {
  validarCampos,
  validarJWT,
  esAdminRole,
  tieneRole,
} = require("../middlewares");

const {
  esRoleValido,
  emailExiste,
  existeUsuarioPorId,
} = require("../helpers/db-validators");

const {
  usuariosGetAll,
  usuariosGet,
  usuariosPut,
  usuariosPost,
  usuariosDelete,
  usuariosPatch,
} = require("../controllers/usuarios");

const router = Router();

router.get("/", usuariosGetAll);
router.get(
  "/:id",
  [
    check("id", "No es un ID válido").isMongoId(),
    validarCampos,
    check("id").custom(existeUsuarioPorId),
    validarCampos,
  ],
  usuariosGet
);

router.put(
  "/:id",
  [
    validarJWT,
    check("id", "No es un ID válido").isMongoId(),
    validarCampos,
    check("id").custom(existeUsuarioPorId),
    check("rol").custom(esRoleValido),
    check("names", "El nombre es obligatorio").not().isEmpty(),

    validarCampos,
  ],
  usuariosPut
);

router.post(
  "/",
  [
    check("names", "El nombre es obligatorio").not().isEmpty(),
    // check("lastNames", "El apellido es obligatorio").not().isEmpty(),
    check("password", "El password debe de ser más de 6 letras").isLength({
      min: 6,
    }),
    check("email", "El email no es válido").isEmail(),
    check("email").custom(emailExiste),
    // check("rol", "No es un rol válido").isIn(["ADMIN_ROLE", "USER_ROLE"]),
    check("rol").custom(esRoleValido),
    validarCampos, //middleware que revisa los errores de las validaciones de los checks, si esto pasa, ejecuta el controlador, sino no ejecuta
  ],
  usuariosPost
);

router.delete(
  "/:id",
  [
    validarJWT,
    // esAdminRole, //A FUERZA PIDE QUE SEA ADMINISTRADOR
    tieneRole("ADMIN_ROLE"), //SON LOS ROLES QUE PODRAN ELIMINAR UN USUARIO
    check("id", "No es un ID válido").isMongoId(),
    validarCampos,
    check("id").custom(existeUsuarioPorId),
    validarCampos,
  ],
  usuariosDelete
);

router.patch("/", usuariosPatch);

module.exports = router;
