const { Router } = require("express");
const { check } = require("express-validator");

const {
  validarJWT,
  validarCampos,
  esAdminRole,
  tieneRole,
} = require("../middlewares");

const {
  crearQuestion,
  obtenerQuestions,
  obtenerQuestion,
  actualizarQuestion,
  borrarQuestion,
} = require("../controllers/questions");
const { existeQuestionPorId } = require("../helpers/db-validators");

const router = Router();

/**
 * {{url}}/api/questions
 */

//  Obtener todas las Questions - publico
router.get("/", obtenerQuestions);

// Obtener una question por id - publico
router.get(
  "/:id",
  [
    check("id", "No es un id de Mongo válido").isMongoId(),
    validarCampos,
    check("id").custom(existeQuestionPorId),
    validarCampos,
  ],
  obtenerQuestion
);

// Crear question - privado - cualquier persona con un token válido
router.post(
  "/",
  [
    validarJWT,
    tieneRole("ADMIN_ROLE"), //SON LOS ROLES QUE PODRAN modificar una question
    check("titleQuestion", "El nombre es obligatorio").not().isEmpty(),
    validarCampos,
  ],
  crearQuestion
);

// Actualizar - privado - cualquiera con token válido
router.put(
  "/:id",
  [
    validarJWT,
    tieneRole("ADMIN_ROLE"), //SON LOS ROLES QUE PODRAN modificar una question
    check("id", "No es un id de Mongo válido").isMongoId(),
    validarCampos,
    check("id").custom(existeQuestionPorId),
    check("titleQuestion", "El nombre es obligatorio").not().isEmpty(),
    validarCampos,
  ],
  actualizarQuestion
);

// Borrar una question - Admin
router.delete(
  "/:id",
  [
    validarJWT,
    // esAdminRole,
    tieneRole("ADMIN_ROLE"), //SON LOS ROLES QUE PODRAN modificar una question
    check("id", "No es un id de Mongo válido").isMongoId(),
    validarCampos,
    check("id").custom(existeQuestionPorId),
    validarCampos,
  ],
  borrarQuestion
);

module.exports = router;
