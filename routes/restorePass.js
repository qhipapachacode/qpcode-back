const { Router } = require("express");
const { restorePass } = require("../controllers/restorePass");

const router = Router();

router.post("/", restorePass);

module.exports = router;
